# Change log

## v2.1.0 (2023-01-21)

* Allow PHP 8.2
* Support doctrine/collections:^2

## v2.0.0 (2022-02-28)

* Allow PHP 8.1
* Support PagerWave 2.x

## v1.1.0 (2020-11-29)

* Allow PHP 8.0.

## v1.0.0 (2020-10-01)

* Split off from main PagerWave repository.
