# pagerwave/doctrine-collections-extension

This package lets you paginate [Doctrine Collections][collections] objects using
[PagerWave][pagerwave].

## Installation

~~~
$ composer require pagerwave/doctrine-collections-extension
~~~

## Usage

~~~php
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use PagerWave\Extension\DoctrineCollections\SelectableAdapter;

// Any collection that implements the Selectable interface works,
// including Doctrine ORM repositories, and persisted collections.
$collection = new ArrayCollection();

// Optionally, provide criteria as the second argument to the adapter.
$criteria = Criteria::create()
    ->where(Criteria::expr()->eq('visible', true));

$adapter = new SelectableAdapter($collection, $criteria);
~~~

Read the [PagerWave documentation][docs] to learn more.

## Licence

This project is released under the Zlib licence.


[collections]: https://www.doctrine-project.org/projects/collections.html
[docs]: https://gitlab.com/pagerwave/PagerWave/-/tree/1.x/docs
[pagerwave]: https://gitlab.com/pagerwave/PagerWave
