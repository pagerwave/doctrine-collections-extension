<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineCollections\Tests\Fixtures;

class Entity
{
    /**
     * @var int
     */
    public $ranking;

    /**
     * @var int
     */
    public $id;

    public function __construct(int $ranking, int $id)
    {
        $this->ranking = $ranking;
        $this->id = $id;
    }
}
