<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineCollections\Tests;

use Doctrine\Common\Collections\ArrayCollection;
use PagerWave\Extension\DoctrineCollections\SelectableAdapter;
use PagerWave\Extension\DoctrineCollections\Tests\Fixtures\Entity;
use PagerWave\Extension\DoctrineCollections\Tests\Fixtures\EntityDefinition;
use PagerWave\Query;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Extension\DoctrineCollections\SelectableAdapter
 */
class SelectableAdapterTest extends TestCase
{
    /**
     * @var SelectableAdapter
     */
    private $adapter;

    protected function setUp(): void
    {
        $this->adapter = new SelectableAdapter(new ArrayCollection([
            new Entity(4, 5),
            new Entity(5, 3),
            new Entity(12, 10),
            new Entity(2, 8),
            new Entity(4, 6),
            new Entity(4, 2),
        ]));
    }

    public function testPaging(): void
    {
        $result = $this->adapter->getResults(5, new EntityDefinition(), new Query());

        $this->assertSame([10, 3, 2, 5, 6], array_column($result->getEntries(), 'id'));
        $this->assertSame(8, $result->getNextEntry()->id);
    }

    public function testPagingWithQuery(): void
    {
        $result = $this->adapter->getResults(5, new EntityDefinition(), new Query([
            'ranking' => 4,
            'id' => 2,
        ]));

        $this->assertSame([2, 5, 6, 8], array_column($result->getEntries(), 'id'));
        $this->assertNull($result->getNextEntry());
    }
}

