<?php

declare(strict_types=1);

namespace PagerWave\Extension\DoctrineCollections;

use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use PagerWave\Adapter\AdapterInterface;
use PagerWave\AdapterResult;
use PagerWave\AdapterResultInterface;
use PagerWave\DefinitionInterface;
use PagerWave\QueryInterface;

final class SelectableAdapter implements AdapterInterface
{
    /**
     * @var Selectable
     */
    private $selectable;

    /**
     * @var Criteria|null
     */
    private $criteria;

    public function __construct(
        Selectable $selectable,
        Criteria $criteria = null
    ) {
        $this->selectable = $selectable;
        $this->criteria = $criteria;
    }

    public function getResults(
        int $max,
        DefinitionInterface $definition,
        QueryInterface $query
    ): AdapterResultInterface {
        $criteria = $this->criteria ? clone $this->criteria : new Criteria();
        $criteria->setMaxResults($max + 1);

        foreach ($definition->getFieldNames() as $fieldName) {
            $desc = $definition->isFieldDescending($fieldName);
            $orders[$fieldName] = $desc ? Criteria::DESC : Criteria::ASC;

            if ($query->isFilled()) {
                $elements[] = [$fieldName, $query->get($fieldName), $desc];
            }
        }

        $criteria->orderBy(array_merge($criteria->getOrderings(), $orders ?? []));

        if (isset($elements)) {
            $this->mangleQuery($criteria, $elements);
        }

        $entries = $this->selectable->matching($criteria)->getValues();
        $nextEntry = \count($entries) > $max ? array_pop($entries) : null;

        return new AdapterResult($entries, $nextEntry);
    }

    /**
     * Add a where-clause to the query like:
     *
     * ~~~
     * (a <= 3) AND (a < 3 OR b >= 4) AND (a < 3 AND b > 4 OR c <= 5)
     * ~~~
     *
     * @param array $elements [field, value, is descending]
     */
    private function mangleQuery(Criteria $qb, array $elements): void
    {
        $i = 0;

        $expr = $qb->expr()->andX(...array_map(static function ($field) use ($elements, $qb, &$i) {
            $prev = array_map(static function ($field) use ($qb) {
                return $qb->expr()->{$field[2] ? 'lt' : 'gt'}($field[0], $field[1]);
            }, array_slice($elements, 0, $i++));

            if ($prev) {
                $expr[] = $qb->expr()->andX(...$prev);
            }

            $expr[] = $qb->expr()->{$field[2] ? 'lte' : 'gte'}($field[0], $field[1]);

            return $qb->expr()->orX(...$expr);
        }, $elements));

        $qb->andWhere($expr);
    }
}
